const express = require("express");
const router = express.Router();
const userController = require("./../api/controllers/user.controller");

router.post("/create", userController.createUser);
module.exports = router;