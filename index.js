const express = require("express");
const app = express();
const userRouter = require('./routing/user.routing');

app.use('/api/user', userRouter);

const server = app.listen(process.env.PORT || 9000, function () {
    console.log(`Ready on port ${server.address().port}`);
})

