const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    email: {type: String, required: true, trim: true, unique: 1},
    arrivalDate: {type: Date, required: true},
    companyName: {type: String, required: true},
    companyPosition: {type: String, required: true},
    role: {type: String, required: true},
    sex: {type: String, required: true},
    birthdate: {type: Date, required: true},
    country: {type: String, required: true},
    password: {type: String, required: true},
    deleted: {type: Boolean, required: false, default: false},
    deletedAt: {type: Date, required: false, default: null}
});

UserSchema.pre("save", function (next) {
    const self = this;
    mongoose.models["User"].findOne({email: this.email}, "email", function (error, result) {
        if (error) {
            next(error);
        } else if (result) {
            const errorMessage = "Email must be unique";
            self.invalidate("email", errorMessage);
            next(new Error(errorMessage));
        } else {
            next();
        }
    });
});

module.exports = mongoose.model("User", UserSchema);